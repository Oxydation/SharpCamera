﻿using System;
using SharpCamera;
using libgphoto2;

namespace ShowWidgetInfo
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            // Try to grab a camera to profile
            TetheredCamera cam = TetheredCamera.GetFirst();
            if (cam == null)
            {
                Console.WriteLine("Not able to find any cameras");
                return;
            }

            // Show some info & connect
            Console.WriteLine("Connecting to {0}", cam.Name);
            cam.Connect();

            PrintWidgetTreeInfo(cam.rootWidget);

            cam.Exit();
        }

        /// <summary>
        /// Print info about the widget tree, this fucntion is recursive.
        /// </summary>
        /// <param name="widget">CameraWidget to print info about</param>
        /// <param name="level"> which level of recursion we are at, used for indenting.</param>
        public static void PrintWidgetTreeInfo(GPhoto2.CameraWidget widget, uint level=0)
        {
            string indent = new string(' ', (int)(level * 2));
            string ro = widget.Readonly ? " [RO]" : string.Empty;
            Console.WriteLine("{0}{1}: {2}{3}", indent, widget.Type, widget.Name, ro);

            switch (widget.Type)
            {
                case GPhoto2.CameraWidgetType.GP_WIDGET_WINDOW:
                case GPhoto2.CameraWidgetType.GP_WIDGET_SECTION:
                    // Print children
                    GPhoto2.ContainerCameraWidget container = (GPhoto2.ContainerCameraWidget)widget;
                    for (int i = 0; i < container.CountChildren; i++)
                        PrintWidgetTreeInfo(container.GetChild(i), level + 1);
                    break;

                case GPhoto2.CameraWidgetType.GP_WIDGET_TEXT:
                    GPhoto2.TextCameraWidget text = (GPhoto2.TextCameraWidget)widget;
                    Console.WriteLine("{0}{1}", indent + "  ", text.Value);
                    break;
                   
                case GPhoto2.CameraWidgetType.GP_WIDGET_DATE:
                    GPhoto2.DateCameraWidget date = (GPhoto2.DateCameraWidget)widget;
                    Console.WriteLine("{0}{1}", indent + "  ", date.Value);
                    break;

                case GPhoto2.CameraWidgetType.GP_WIDGET_RANGE:
                    GPhoto2.RangeCameraWidget range = (GPhoto2.RangeCameraWidget)widget;
                    Console.WriteLine("{0}{1}", indent + "  ", range.Value);
                    Console.WriteLine("{0}{1}", indent + "  ", range.Range);
                    break;

                case GPhoto2.CameraWidgetType.GP_WIDGET_TOGGLE:
                    GPhoto2.ToggleCameraWidget toggle = (GPhoto2.ToggleCameraWidget)widget;
                    Console.WriteLine("{0}{1}", indent + "  ", toggle.Value ? "On" : "Off");
                    break;

                case GPhoto2.CameraWidgetType.GP_WIDGET_RADIO:
                    GPhoto2.RadioCameraWidget radio = (GPhoto2.RadioCameraWidget)widget;
                    Console.WriteLine("{0}{1}", indent + "  ", radio.Value);
                    Console.WriteLine("{0}({1})", indent + "  ", string.Join(", ", radio.Choices));
                    break;

                case GPhoto2.CameraWidgetType.GP_WIDGET_MENU:
                    GPhoto2.MenuCameraWidget menu = (GPhoto2.MenuCameraWidget)widget;
                    Console.WriteLine("{0}{1}", indent + "  ", menu.Value);
                    Console.WriteLine("{0}({1})", indent + "  ", string.Join(", ", menu.Choices));
                    break;
            }
        }
    }
}
