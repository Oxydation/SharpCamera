﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A specific type of GPhoto2 exception when a camera widget ins't found.

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        /// <summary>
        /// This is an execption that is only thrown when trying to querey for a CameraWidget.
        /// 
        /// The underlying GPhoto2Exception will always be one with `GP_ERROR_BAD_PARAMETERS`
        /// </summary>
        public class WidgetNotFoundException : GPhoto2Exception
        {
            /// <summary>
            /// Gets the details of the exection.  Always incoded as a string.
            /// </summary>
            /// <value>The details.</value>
            public string Details { get; private set; }

            /// <summary>
            /// Creates a new WidgetNotFound.
            /// </summary>
            /// <param name="details">Details about the widget that wasn't found.</param>
            public WidgetNotFoundException(string details)
                : base(GP_ERROR_BAD_PARAMETERS)
            {
                this.Details = details;
            }

            /// <summary>
            /// Creates a new WidgetNotFound error with a message attached.
            /// </summary>
            /// <param name="message">A message to put into the error</param>
            /// <param name="details">Details about the widget that wasn't found.</param>
            public WidgetNotFoundException(string message, string details)
                : base(message, GP_ERROR_BAD_PARAMETERS)
            {
                this.Details = details;
            }

            /// <summary>
            /// Creates a new WidgetNotFound error with a message attached and an inner error.
            /// </summary>
            /// <param name="message">A message to put into the error</param>
            /// <param name="inner">The exception that occured inside of this one</param>
            /// <param name="details">Details about the widget that wasn't found.</param>
            public WidgetNotFoundException(string message, Exception inner, string details)
                : base(message, inner, GP_ERROR_BAD_PARAMETERS)
            {
                this.Details = details;
            }
        }
    }
}
