﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Data port types.

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        public enum GPPortType
        {
            GP_PORT_NONE            =      0,   //  No specific type associated.
            GP_PORT_SERIAL          = 1 << 0,   // Serial port.
            GP_PORT_USB             = 1 << 2,   // USB port.
            GP_PORT_DISK            = 1 << 3,   // Disk / local mountpoint port.
            GP_PORT_PTPIP           = 1 << 4,   // PTP/IP port.
            GP_PORT_USB_DISK_DIRECT = 1 << 5,   // Direct IO to an usb mass storage device.
            GP_PORT_USB_SCSI        = 1 << 6    // USB Mass Storage raw SCSI port.
        }
    }
}

