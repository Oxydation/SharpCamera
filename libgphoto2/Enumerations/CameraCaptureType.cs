﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Capture types from the camera.

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        public enum CameraCaptureType
        {
            GP_CAPTURE_IMAGE,
            GP_CAPTURE_MOVIE,
            GP_CAPTURE_SOUND
        }
    }
}

