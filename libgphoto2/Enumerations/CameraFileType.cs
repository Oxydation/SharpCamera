﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Camera file types.

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        public enum CameraFileType
        {
            GP_FILE_TYPE_PREVIEW,
            GP_FILE_TYPE_NORMAL,
            GP_FILE_TYPE_RAW,
            GP_FILE_TYPE_AUDIO,
            GP_FILE_TYPE_EXIF,
            GP_FILE_TYPE_METADATA
        }
    }
}

