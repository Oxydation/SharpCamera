﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  C# friendly bindings to libgphoto2.

using System;

namespace libgphoto2
{
    /// <summary>
    /// C# friendly bindings to libgphoto2.
    /// </summary>
    public static partial class GPhoto2
    {
        // Hi mom!
    }
}
