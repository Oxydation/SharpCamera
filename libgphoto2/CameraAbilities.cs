﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Abilities about a camera.

using System;
using System.Runtime.InteropServices;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Ansi)]
        public struct CameraAbilities
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst=128)]
            public sbyte[] model;

            public CameraDriverStatus status;
            public GPPortType port;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst=64)]
            public int[] speed;

            public CameraOperation operations;
            public CameraFileOperation file_operations;
            public CameraFolderOperation folder_operations;
            public int usb_vendor;
            public int usb_product;
            public int usb_class;
            public int usb_subclass;
            public int usb_protocol;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst=1024)]
            public sbyte[] library;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst=1024)]
            public sbyte[] id;

            public GphotoDeviceType device_type;

            // reserved space
            int reserved2;
            int reserved3;
            int reserved4;
            int reserved5;
            int reserved6;
            int reserved7;
            int reserved8;
        }
    }
}

