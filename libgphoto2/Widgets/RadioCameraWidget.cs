﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A CameraWidget that has multiple options, but only one can be active at a time; think "drop down box".

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        /// <summary>
        /// A CameraWidget that has multiple selections
        /// </summary>
        public class RadioCameraWidget : ChoicesCameraWidget
        {
            /// <summary>
            /// Create a widget that is a multiple choice selection (think drop down widget), from a previously
            /// instantiated object.
            /// </summary>
            /// <param name="widget">handle to a Radio widget.</param>
            public RadioCameraWidget(IntPtr widget)
                : base(widget)
            {
                if (Type != CameraWidgetType.GP_WIDGET_RADIO)
                    throw new ArgumentException("supplied handle is not for a Radio widget");
            }

            /// <summary>
            /// Create a brand new Radio widget.
            /// </summary>
            /// <param name="label">Label for the widget.</param>
            public RadioCameraWidget(string label)
            {
                int ok = gp_widget_new(CameraWidgetType.GP_WIDGET_RADIO, label, out handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("error creating new Radio widget", ok);
            }
        }
    }
}

