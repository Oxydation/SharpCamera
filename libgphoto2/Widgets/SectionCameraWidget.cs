﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A Widget with child widgets.

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        /// <summary>
        /// A widget containing other ones (think "tab").
        /// </summary>
        public class SectionCameraWidget : ContainerCameraWidget
        {
            /// <summary>
            /// Create a Section widget, from a previously instatitated object.
            /// </summary>
            /// <param name="widget">handle to the Widget, must be of the Section type</param>
            public SectionCameraWidget(IntPtr widget)
                : base(widget)
            {
                if (Type != CameraWidgetType.GP_WIDGET_SECTION)
                    throw new ArgumentException("supplied handle is not for a Section widget");
            }

            /// <summary>
            /// Create a branch new Section widget.
            /// </summary>
            /// <param name="label">Label to give the widget.</param>
            public SectionCameraWidget(string label)
            {
                int ok = gp_widget_new(CameraWidgetType.GP_WIDGET_SECTION, label, out handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("error creating new Section widget", ok);
            }
        }
    }
}

