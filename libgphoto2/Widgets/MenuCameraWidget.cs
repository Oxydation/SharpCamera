﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A CameraWidget that is a menu.

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        /// <summary>
        /// A CameraWidget that is a Menu.
        /// </summary>
        public class MenuCameraWidget : ChoicesCameraWidget
        { 
            /// <summary>
            /// Creates a MenuWidget, from a previously instatited object.
            /// </summary>
            /// <param name="widget">handle to a Widget (that needs to be of a Menu Type).</param>
            public MenuCameraWidget(IntPtr widget)
                : base(widget)
            {
                if (Type != CameraWidgetType.GP_WIDGET_MENU)
                    throw new ArgumentException("supplied handle is not for a Menu widget");
            }

            /// <summary>
            /// Create a brand new Menu widget.
            /// </summary>
            /// <param name="label">Label to give to the menu.</param>
            public MenuCameraWidget(string label)
            {
                int ok = gp_widget_new(CameraWidgetType.GP_WIDGET_MENU, label, out handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("error creating new Menu widget", ok);
            }
        }
    }
}

