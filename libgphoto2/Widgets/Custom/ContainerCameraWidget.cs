﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A CameraWidget that can have children.

using System;
using System.Runtime.InteropServices;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        #region P/Invoke Functions
        [DllImport("gphoto2")]
        private static extern int gp_widget_append(IntPtr widget, IntPtr child);

        [DllImport("gphoto2")]
        private static extern int gp_widget_prepend(IntPtr widget, IntPtr child);

        [DllImport("gphoto2")]
        private static extern int gp_widget_count_children(IntPtr widget);

        [DllImport("gphoto2")]
        private static extern int gp_widget_get_child(IntPtr widget, int child_number, out IntPtr child);

        [DllImport("gphoto2", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        private static extern int gp_widget_get_child_by_label(
            IntPtr widget,
            [MarshalAs(UnmanagedType.LPStr)] string label,
            out IntPtr child
        );

        [DllImport("gphoto2")]
        private static extern int gp_widget_get_child_by_id(IntPtr widget, int id, out IntPtr child);

        [DllImport("gphoto2", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        private static extern int gp_widget_get_child_by_name(
            IntPtr widget,
            [MarshalAs(UnmanagedType.LPStr)] string name,
            out IntPtr child
        );
        #endregion

        /// <summary>
        /// A CameraWidget that can have children.
        /// 
        /// This acts as a base class for SectionCameraWidget and WindowCameraWidget
        /// </summary>
        public class ContainerCameraWidget : CameraWidget
        {
            /// <summary>
            /// Create a CameraWidget that can have children.
            /// </summary>
            /// <param name="widget">handle to the widget, needs to be a Section or Window widget.</param>
            internal ContainerCameraWidget(IntPtr widget)
                : base(widget)
            {
                if ((Type != CameraWidgetType.GP_WIDGET_SECTION) && (Type != CameraWidgetType.GP_WIDGET_WINDOW))
                    throw new ArgumentException("supplied handle is not a Section or Window widget");
            }

            internal ContainerCameraWidget() { }

            /// <summary>
            /// Append a widget to the end
            /// </summary>
            /// <param name="widget">Widget to append.</param>
            public void Append(CameraWidget widget)
            {
                int ok = gp_widget_append(handle, widget.handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to append widget", ok);
            }

            /// <summary>
            /// Prepend the specified widget to the front.
            /// </summary>
            /// <param name="widget">Widget to prepend.</param>
            public void Prepend(CameraWidget widget)
            {
                int ok = gp_widget_prepend(handle, widget.handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to prepend widget", ok);
            }

            /// <summary>
            /// Counts the children of the Container Widget
            /// </summary>
            /// <value>The number of children.</value>
            public int CountChildren
            {
                get
                {
                    int num = gp_widget_count_children(handle);
                    if (num < 0)
                        throw new GPhoto2Exception("Not able to get number of children", num);

                    return num;
                }
            }

            /// <summary>
            /// Get a child widget at the provided index.
            /// 
            /// This only checks immediate children.
            /// </summary>
            /// <returns>The child widget.</returns>
            /// <param name="childNumber">index of the widget</param>
            public CameraWidget GetChild(int childNumber)
            {
                IntPtr child;
                int ok = gp_widget_get_child(handle, childNumber, out child);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to get child at provided index", ok);

                return NewFromType(child);
            }

            // TODO this needs to be tested
            /// <summary>
            /// Get a child widget that has the provided label.
            /// 
            /// This function is recursive and will check the widget tree starting at this node.
            /// </summary>
            /// <returns>The child widget that has the label.</returns>
            /// <param name="label">label that the child needs to have</param>
            public CameraWidget GetChildByLabel(string label)
            {
                IntPtr widgetPtr;
                int ok = gp_widget_get_child_by_label(handle, label, out widgetPtr);
                if (ok == GP_ERROR_BAD_PARAMETERS)
                    throw new WidgetNotFoundException(label);    // Assume bad label
                else if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to retrive child with label", ok);

                return NewFromType(widgetPtr);
            }

            // TODO this needs to be tested
            /// <summary>
            /// Get a child widget that has the provided Id.
            /// 
            /// This function is recursive and will check the widget tree starting at this node.
            /// </summary>
            /// <returns>The child widget that has the identifier.</returns>
            /// <param name="id">Id number that the child needs to have</param>
            public CameraWidget GetChildById(int id)
            {
                IntPtr widgetPtr;
                int ok = gp_widget_get_child_by_id(handle, id, out widgetPtr);
                if (ok == GP_ERROR_BAD_PARAMETERS)
                    throw new WidgetNotFoundException(id.ToString());    // Assume bad id
                else if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to retrive child with label", ok);

                return NewFromType(widgetPtr);
            }

            // TODO this needs to be tested
            /// <summary>
            /// Get a child widget that has the provided name.
            /// 
            /// This function is recursive and will check the widget tree starting at this node.
            /// </summary>
            /// <returns>The child widget that has the name.</returns>
            /// <param name="name">name that the child needs to have</param>
            public CameraWidget GetChildByName(string name)
            {
                IntPtr widgetPtr;
                int ok = gp_widget_get_child_by_name(handle, name, out widgetPtr);
                if (ok == GP_ERROR_BAD_PARAMETERS)
                    throw new WidgetNotFoundException(name);    // Assume bad name
                else if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to retrive child with name", ok);

                return NewFromType(widgetPtr);
            }
        }
    }
}

