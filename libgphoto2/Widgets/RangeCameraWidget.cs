﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A Range type CameraWidget
using System;
using System.Runtime.InteropServices;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        #region P/Invoke Functions
        [DllImport("gphoto2")]
        private static extern int gp_widget_get_range(IntPtr range, out float min, out float max, out float increment);

        [DllImport("gphoto2")]
        private static extern int gp_widget_set_range(IntPtr range, float min, float max, float increment);

        [DllImport("gphoto2", EntryPoint="gp_widget_get_value")]
        private static extern int gp_widget_get_value_float(IntPtr widget, out float value);

        [DllImport("gphoto2", EntryPoint="gp_widget_set_value")]
        private static extern int gp_widget_set_value_float(IntPtr widget, float value);
        #endregion


        /// <summary>
        /// This is a handy and little structure to describe the range parameters for a RangeWidget.
        /// 
        /// This is not in the original libgphoto2 library, it's for the C# bindings only.
        /// </summary>
        public struct Range
        {
            /// <summary>
            /// The minimum.
            /// </summary>
            public float Min { get; set; }

            /// <summary>
            /// The max.
            /// </summary>
            public float Max { get; set; }

            /// <summary>
            /// The increment.
            /// </summary>
            public float Increment { get; set; }

            /// <summary>
            /// Range in a string format.
            /// </summary>
            /// <returns>A <see cref="System.String"/> that represents the current <see cref="libgphoto2.GPhoto2.Range"/>.</returns>
            public override string ToString()
            {
                return string.Format("[{0}, {1}] +-{2}", Min, Max, Increment);
            }
        }


        /// <summary>
        /// A Camera widget that is a range of values (a slider).
        /// </summary>
        public class RangeCameraWidget : CameraWidget
        {
            /// <summary>
            /// Create a widget that acts as a slider, from a previously instantiated object.
            /// </summary>
            /// <param name="widget">handle to a Range widget.</param>
            public RangeCameraWidget(IntPtr widget)
                : base(widget)
            {
                if (Type != CameraWidgetType.GP_WIDGET_RANGE)
                    throw new ArgumentException("Supplied handle is not a Range widget");
            }

            /// <summary>
            /// Create a branch new Range widget.
            /// </summary>
            /// <param name="label">Label to give the widget.</param>
            public RangeCameraWidget(string label)
            {
                int ok = gp_widget_new(CameraWidgetType.GP_WIDGET_RANGE, label, out handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("error creating new Range widget", ok);
            }

            /// <summary>
            /// Get or set the value of the range widget.
            /// </summary>
            /// <value>The value, which should be between (or at) the Low and High values</value>
            public float Value
            {
                get
                {
                    float value;
                    int ok = gp_widget_get_value_float(handle, out value);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Not able to get range value", ok);

                    return value;
                }

                set
                {
                    int ok = gp_widget_set_value_float(handle, value);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Not able to set range value", ok);
                }
            }
      
            /// <summary>
            /// Gets or sets the Range of this widget.
            /// 
            /// If you want to set this, you'll need to use the custom Range structure that was created for this binding.
            /// It would be best to call the getter of this property first, then change whatever parameter you want, then
            /// set it with that Range.  e.g:
            /// 
            ///    Range r = hueAdjusment.Range;
            ///    r.Increment = 0.5;
            ///    hueAdjustment.Range = r;
            /// </summary>
            /// <value>A Range structure.</value>
            public Range Range
            {
                get
                {
                    float min, max, increment;
                    int ok = gp_widget_get_range(handle, out min, out max, out increment);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Not able to get widget range values", ok);

                    return new Range()
                    {
                        Min = min,
                        Max = max,
                        Increment = increment
                    };
                }

                set
                {
                    int ok = gp_widget_set_range(handle, value.Min, value.Max, value.Increment);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Not able to set range values", ok);
                }
            }
        }
    }
}

