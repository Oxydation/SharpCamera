﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A list of camera abilities.

using System;
using System.Runtime.InteropServices;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        #region P/Invoke Functions
        [DllImport("gphoto2")]
        private static extern int gp_abilities_list_new(ref IntPtr list);

        [DllImport("gphoto2")]
        private static extern int gp_abilities_list_free(IntPtr list);

        [DllImport("gphoto2")]
        private static extern int gp_abilities_list_load(IntPtr list, IntPtr context);

        [DllImport("gphoto2")]
        // int gp_abilities_list_detect (CameraAbilitiesList *list,
        //     GPPortInfoList *info_list, CameraList *l,
        //     GPContext *context);
        private static extern int gp_abilities_list_detect(IntPtr list, IntPtr info_list, IntPtr l, IntPtr context);

        [DllImport("gphoto2")]
        private static extern int gp_abilities_list_get_abilities(IntPtr list, int index, out CameraAbilities abilities);
        #endregion


        public class CameraAbilitiesList : IDisposable
        {
            #region Data
            private bool disposed = false;
            public IntPtr handle;    // CameraAbilitiesList *
            private GPContext context;
            #endregion


            public CameraAbilitiesList() {
                int ok = gp_abilities_list_new(ref handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to create new CameraAbilitiesList", ok);
            }

            ~CameraAbilitiesList() {
                Dispose(false);
            }

            public void Dispose() {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// Does the actual cleanup work
            protected virtual void Dispose(bool disposing)
            {
                if (disposed)
                    return;

                // Free Managed
                if (disposing)
                {
                }

                // Free unmanaged
                gp_abilities_list_free(handle);

                disposed = true;
            }

            /// <summary>
            /// Scan the sytem for Camera drivers
            /// </summary>
            /// <param name="context">Context.</param>
            public void Load(GPContext context) {
                int ok = gp_abilities_list_load(handle, context.handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to scan system for Camera drivers", ok);

                this.context = context;
            }

            // TODO C# friendly type?
            public CameraList Detect(GPPortInfoList infoList) {
                // TODO other type of custom exception?
                if (context == null)
                    throw new Exception("Need context attached before detection, call Load() method first.");

                CameraList camList = new CameraList();
                int ok = gp_abilities_list_detect(handle, infoList.handle, camList.handle, context.handle); // TODO hangs here...
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Failure when trying to detect cameras", ok);

                return camList;
            }

            /// <summary>
            /// Retrives the abilities of a camera (in the list) at a given index.
            /// </summary>
            /// <returns>The abilities of the camera</returns>
            /// <param name="index">index to look at</param>
            public CameraAbilities GetAbilities(int index)
            {
                CameraAbilities abilities;
                int ok = gp_abilities_list_get_abilities(handle, index, out abilities);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to get CameraAbilities", ok);

                return abilities;
            }
        }
    }
}

