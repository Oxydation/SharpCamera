﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Files on the Camera.

using System;
using System.Runtime.InteropServices;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        #region P/Invoke Functions
        [DllImport("gphoto2")]
        private static extern int gp_file_new(ref IntPtr file);

        [DllImport("gphoto2")]
        private static extern int gp_file_free(IntPtr file);

        [DllImport("gphoto2")]
        private static extern int gp_file_get_data_and_size(IntPtr file, ref IntPtr data, ref ulong size);
        #endregion


        /// <summary>
        /// A CameraFile object
        /// </summary>
        public class CameraFile : IDisposable {
            private bool disposed = false;
            public IntPtr handle;    // CameraFile *

            public CameraFile() {
                int ok = gp_file_new(ref handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to create new CameraFile", ok);
            }

            /// <summary>
            /// Create a CameraFile object from a pre-allocated file.
            /// </summary>
            /// <param name="file">handle to the CameraFile</param>
            public CameraFile(IntPtr file)
            {
                handle = file;
            }

            ~CameraFile() {
                Dispose(false);
            }

            public void Dispose() {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// Does the actual cleanup work
            protected virtual void Dispose(bool disposing)
            {
                if (disposed)
                    return;

                // Free Managed
                if (disposing)
                {
                }

                // Free unmanaged
                gp_file_free(handle);

                disposed = true;
            }

            // Retrives the data of the file off of the camera
            // Equivilent to `gp_file_get_data_and_size()` 
            public byte[] Get()
            {
                // Get the data
                ulong size = 0;
                IntPtr data = IntPtr.Zero;
                int ok = gp_file_get_data_and_size(handle, ref data, ref size);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to get size or data from preview image.", ok);

                // Convert
                // TODO leaking?
                byte[] bytes = new byte[size];
                Marshal.Copy(data, bytes, 0, bytes.Length);
                return bytes;
            }
        }
    }
}

