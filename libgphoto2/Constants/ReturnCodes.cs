﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Return values from functions.

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        public const int GP_OK = 0;
        public const int GP_ERROR = -1;
        public const int GP_ERROR_BAD_PARAMETERS = -2;
        public const int GP_ERROR_NO_MEMORY = -3;
        public const int GP_ERROR_LIBRARY = -4;
        public const int GP_ERROR_UNKNOWN_PORT = -5;
        public const int GP_ERROR_NOT_SUPPORTED = -6;
        public const int GP_ERROR_IO = -7;
        public const int GP_ERROR_FIXED_LIMIT_EXCEEDED = -8;
        public const int GP_ERROR_TIMEOUT = -10;
        public const int GP_ERROR_IO_SUPPORTED_SERIAL = -20;
        public const int GP_ERROR_IO_SUPPORTED_USB = -21;
        public const int GP_ERROR_IO_INIT = -31;
        public const int GP_ERROR_IO_READ = -34;
        public const int GP_ERROR_IO_WRITE = -35;
        public const int GP_ERROR_IO_UPDATE = -37;
        public const int GP_ERROR_IO_SERIAL_SPEED = -41;
        public const int GP_ERROR_IO_USB_CLEAR_HALT = -51;
        public const int GP_ERROR_IO_USB_FIND = -52;
        public const int GP_ERROR_IO_USB_CLAIM = -53;
        public const int GP_ERROR_IO_LOCK = -60;
        public const int GP_ERROR_HAL = -70;

        public const int GP_ERROR_CORRUPTED_DATA = -102;
        public const int GP_ERROR_FILE_EXISTS = -103;
        public const int GP_ERROR_MODEL_NOT_FOUND = -105;
        public const int GP_ERROR_DIRECTORY_NOT_FOUND = -107;
        public const int GP_ERROR_FILE_NOT_FOUND = -108;
        public const int GP_ERROR_DIRECTORY_EXISTS = -109;
        public const int GP_ERROR_CAMERA_BUSY = -110;
        public const int GP_ERROR_PATH_NOT_ABSOLUTE = -111;
        public const int GP_ERROR_CANCEL = -112;
        public const int GP_ERROR_CAMERA_ERROR = -113;
        public const int GP_ERROR_OS_FAILURE = -114;
        public const int GP_ERROR_NO_SPACE = -115;
    }
}

