﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Data structure for getting files on the camera.

using System;
using System.Runtime.InteropServices;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Ansi)]
        public struct CameraFilePath
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst=128)]
            public sbyte[] name;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst=1024)]
            public sbyte[] folder;

            #region C# Friendly Properties
            public String Name
            {
                get { return Utils.SByteArrayToString(name); }
            }

            public String Folder
            {
                get { return Utils.SByteArrayToString(folder); }
            }
            #endregion
        }
    }
}

