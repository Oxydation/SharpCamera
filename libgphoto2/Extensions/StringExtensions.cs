﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Handy extension methods for System.String.

using System;

namespace libgphoto2
{
    public static class StringExtensions
    {
        public static sbyte[] ToSByteArray(this string str)
        {
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(str);
            return Array.ConvertAll(bytes, x => Convert.ToSByte(x));
        }
    }
}

