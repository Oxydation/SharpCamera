﻿// License:      MIT
// Author:       Benjamin N. Summerton <https://16bpp.net>

using System;
using Gtk;

namespace LivePreviewGtk
{
    class Program
    {
        public static void Main(string[] args)
        {
            Application.Init();
            LiveCameraPreviewWindow win = new LiveCameraPreviewWindow();
            win.Show();
            Application.Run();
        }
    }
}
