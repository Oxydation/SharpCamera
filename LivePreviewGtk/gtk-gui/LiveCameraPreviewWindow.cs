
// This file has been generated by the GUI designer. Do not modify.

public partial class LiveCameraPreviewWindow
{
	private global::Gtk.VBox vbox1;
	
	private global::Gtk.Image previewImage;
	
	private global::Gtk.Table table1;

	protected virtual void Build ()
	{
		global::Stetic.Gui.Initialize (this);
		// Widget LiveCameraPreviewWindow
		this.WidthRequest = 1200;
		this.HeightRequest = 800;
		this.Name = "LiveCameraPreviewWindow";
		this.Title = global::Mono.Unix.Catalog.GetString ("Live Preview (w/ GTK#)");
		this.WindowPosition = ((global::Gtk.WindowPosition)(4));
		this.AllowGrow = false;
		// Container child LiveCameraPreviewWindow.Gtk.Container+ContainerChild
		this.vbox1 = new global::Gtk.VBox ();
		this.vbox1.Name = "vbox1";
		this.vbox1.Spacing = 6;
		// Container child vbox1.Gtk.Box+BoxChild
		this.previewImage = new global::Gtk.Image ();
		this.previewImage.Name = "previewImage";
		this.vbox1.Add (this.previewImage);
		global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.vbox1 [this.previewImage]));
		w1.Position = 0;
		// Container child vbox1.Gtk.Box+BoxChild
		this.table1 = new global::Gtk.Table (((uint)(3)), ((uint)(3)), false);
		this.table1.Name = "table1";
		this.table1.RowSpacing = ((uint)(6));
		this.table1.ColumnSpacing = ((uint)(6));
		this.vbox1.Add (this.table1);
		global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(this.vbox1 [this.table1]));
		w2.Position = 1;
		this.Add (this.vbox1);
		if ((this.Child != null)) {
			this.Child.ShowAll ();
		}
		this.DefaultWidth = 1200;
		this.DefaultHeight = 800;
		this.Show ();
	}
}
