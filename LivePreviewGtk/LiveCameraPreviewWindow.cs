﻿// License:      MIT
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Connect to a camera and show a live feed.

using System;
using System.Timers;
using System.Collections.Generic;
using Gtk;
using Gdk;
using SharpCamera;

public partial class LiveCameraPreviewWindow: Gtk.Window
{
    private TetheredCamera cam;
    private Timer previewTimer;
    private Timer scanTimer;

    public LiveCameraPreviewWindow()
        : base(Gtk.WindowType.Toplevel)
    {
        Build();
        Destroyed += new EventHandler(OnDestroy);

        // Show previews
        previewTimer = new Timer(1000.0 / 60.0);       // 60 FPS    // TODO lower FPS?
        previewTimer.Elapsed += displayPreview;

        // Create the scannner
        scanTimer = new Timer(1000.0 / 5.0);    // Scan 5x a second
        scanTimer.Elapsed += scanCameraConnections;
        scanTimer.Start();
    }

    protected void OnDestroy(object sender, EventArgs a)
    {
        // Stop the timers
        scanTimer.Stop();
        previewTimer.Stop();

        // Close camera connection
        if (cam != null)
            cam.Exit();

        Application.Quit();
    }

    protected override bool OnKeyReleaseEvent(EventKey e)
    {
        if (e.Key == Gdk.Key.Escape)
            Destroy();

        return true;
    }

    private void displayPreview(object sender, ElapsedEventArgs e) 
    {
        // show a preview on the image
        Pixbuf pb = new Pixbuf(cam.PreviewAsBytes());
        previewImage.Pixbuf = pb;

        // Have to clean up the image after showing it
        pb.Dispose();
    }

    private void scanCameraConnections(object sender, ElapsedEventArgs e)
    {
        if (cam == null)
        {
            // (Try to) find a new one
            cam = TetheredCamera.GetFirst();
            cam.Connect();
            previewTimer.Start();
        }
        else
        {
            // See if the current one is still there
            if (!cam.Connected)
            {
                previewTimer.Stop();

                // Clear it out
                cam.Exit();
                cam = null;
            }
        }
    }
}
