SharpCamera
===========

- `libgphoto2` [![0.3](https://badge.fury.io/nu/libgphoto2.svg)](https://badge.fury.io/nu/libgphoto2)
- `SharpCamera` [![0.3.5](https://badge.fury.io/nu/SharpCamera.svg)](https://badge.fury.io/nu/SharpCamera)



SharpCamera is a C# interface into using tethered cameras via `libgphoto2` under
the hood.  This project also includes C# friendly bindings to libgphoto2,
but it's recommended you don't use them unless you know what you're doing, they
are incomplete as it is anyways, but more will be added in later.

SharpCamera and the libgphoto2 bindings are licensed under the terms of the GNU
LGPLv3.  See the file `LICENSE-LGPLv3.txt` for more details.

I don't have much documentation up at the moment, so feel free to ask me
something on the issue tracker.  Same goes for if you'd like a feature added.

Since this softare is still pre-1.0, it's API is subject to radical change at
any time.  This library was built to support Moira.  So the development of this
project will depend upon that.


### How to Use & Dependencies

- **If you are using SharpCamera, you'll need to copy the C# bindings of
  libgphoto2 to the same directory as `SharpCamera.dll`** (i.e. your build
  directory).  This is pretty easy to do.  In the project where you are using
  SharpCamera, expand the `References` (might be under the `From Packages`
  section).  For the `libgphoto2` reference, make sure that the `Local Copy`
  options is set to `True` (or check in MonoDevelop).  I'm still pretty new to
  NuGet packaging, so I'm trying to figure out a way for this to automatically
  happen.  Any help would be appreciated.
- You need the native C libgphoto2 DLL (or shared object) on the machine where
  you want to use this library
  - This should also require `libusb` too
  - On Linux you can get this (probably) from your package manager
  - On OS X this can be grabbed using Homebrew

#### Running on Windows

These two libraries are activley developed on a Linux system (and tested on an
OS X machine).  Theoretically SharpCamera and the libgphoto2 bindings should
work on Windows, but I have yet to build & install the libgphoto2 native C
library on the Microsoft platform.

I actually appreciate some help with this at the moment.  If you want to, here
would be the best place to start:
https://github.com/gphoto/libgphoto2/issues/97#issuecomment-353842267

It would be nice in the end to be able to put the native DLL into the NuGet
package as well.


### SimpleCapture & LivePreviewGtk

These two projects are some sample code of how to use SharpCamera.  They are
both under the MIT license.  See the file `LICENSE-MIT.txt` for more details.

`SimpleCapture` is how to simply scan for cameras, establish a connection to the
first one, and take a photo with it.

`LivePreviewGtk` goes a little further and gets a live preview image from a
Camera in a GTK2 window (using GTK#).
